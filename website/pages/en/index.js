/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

class HomeSplash extends React.Component {
  render() {
    const {siteConfig, language = ''} = this.props;
    const {baseUrl, docsUrl} = siteConfig;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

    const SplashContainer = props => (
      <div className="homeContainer">
        <div className="homeSplashFade">
          <div className="wrapper homeWrapper">{props.children}</div>
        </div>
      </div>
    );

    const Logo = props => (
      <div className="projectLogo">
        <img src={props.img_src} alt="Project Logo" />
      </div>
    );

    const ProjectTitle = () => (
      <h2 className="projectTitle" style={{fontFamily: 'Open Sans Light'}}>
        {siteConfig.title}
      </h2>
    );

    const PromoSection = props => (
      <div className="section promoSection">
        <div className="promoRow">
          <div className="pluginRowBlock">{props.children}</div>
        </div>
      </div>
    );

    const Button = props => (
      <div className="pluginWrapper buttonWrapper">
        <a className="button" href={props.href} target={props.target}>
          {props.children}
        </a>
      </div>
    );

    return (
      <SplashContainer>
        <Logo img_src={`${baseUrl}img/o20/ms-office.svg`} />
        <div className="inner">
          <ProjectTitle siteConfig={siteConfig} />
	  <PromoSection>
	  </PromoSection>
          <PromoSection>
	    <Button href="https://flathub.org/apps/details/io.gitlab.o20.word">Get O20 on Flathub</Button>
	    <Button href="https://snapcraft.io/office20">Get O20 on the Snap Store</Button>
          </PromoSection>
          <PromoSection>
            <Button href="https://gitlab.com/abstractsoftware/o20/o20coreapps">Get the Code</Button>
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

class Index extends React.Component {
  render() {
    const {config: siteConfig, language = ''} = this.props;
    const {baseUrl} = siteConfig;

    const Block = props => (
      <Container
        padding={['bottom', 'top']}
        id={props.id}
        background={props.background}>
        <GridBlock
          align="center"
          contents={props.children}
          layout={props.layout}
        />
      </Container>
    );

    /*const FeatureCallout = () => (
      <div
        className="productShowcaseSection paddingBottom"
        style={{textAlign: 'center'}}>
        <h2>Made for Linux</h2>
        <MarkdownBlock>Written in Qt5 and QtQuick and based on KDE, O20 works, looks, and install the same on every Linux OS thanks to Flatpak and Snap.</MarkdownBlock>
        <p><a href='https://flathub.org/apps/details/io.gitlab.o20.word'>Get O20 on Flathub</a></p>
      </div>
    );*/

    const TryOut = () => (
      <Block id="try">
        {[
          {
            content:
              'O20.Word uses the universal ODT and XHTML document formats, which most office suites, including ' +
              'Office365, Office Online, Google Docs, and LibreOffice support.',
            //image: `${baseUrl}img/undraw_code_review.svg`,
            imageAlign: 'left',
            title: 'Document Format Support',
          },
        ]}
      </Block>
    );

    const FeatureCallout = () => (
      <Block background="dark">
        {[
          {
            content: '<MarkdownBlock>Written in Qt5 and QtQuick and based on KDE, O20 works, looks, and install the same on every Linux OS thanks to Flatpak and Snap.</MarkdownBlock>'+
		  '<p><a href="https://flathub.org/apps/details/io.gitlab.o20.word">Get O20 on Flathub</a></p>',
            image: `${baseUrl}img/undraw_note_list.svg`,
            imageAlign: 'right',
            title: 'Made for Linux',
          },
        ]}
      </Block>
    );

	  /*
    const Description = () => (
      <Block background="dark">
        {[
          {
            content:
              'O20.Word uses the simple and lightweight ODT and XHTML document formats, and ' +
              'has with syntax highlighting for over 2000 programming languages, so both writers and developers can' +
              'put it to use.',
            image: `${baseUrl}img/undraw_note_list.svg`,
            imageAlign: 'right',
            title: 'Description',
          },
        ]}
      </Block>
    );

    const LearnHow = () => (
      <Block background="light">
        {[
          {
            content:
              'Each new Docusaurus project has **randomly-generated** theme colors.',
            image: `${baseUrl}img/undraw_youtube_tutorial.svg`,
            imageAlign: 'right',
            title: 'Randomly Generated Theme Colors',
          },
        ]}
      </Block>
    );*/

    const Features = () => (
      <Block layout="fourColumn">
        {[
          {
             content: 'Edit documents, books, reports, and code with ease in an familiar interface.',
            image: `${baseUrl}img/o20/ms-word.svg`,
            imageAlign: 'top',
            title: '<p style="font-family: \'Open Sans Light\'">O20.Word</p>',
          },
          {
            content: 'Keep your notes organized on Linux.',
            image: `${baseUrl}img/o20/ms-onenote.svg`,
            imageAlign: 'top',
            title: '<p style="font-family: \'Open Sans Light\'">O20.Notebook</p>',
          },
          {
            content: 'Coming soon...',
            image: `${baseUrl}img/o20/ms-powerpoint.svg`,
            imageAlign: 'top',
            title: '<p style="font-family: \'Open Sans Light\'">O20.SlideShow</p>',
          }
        ]}
      </Block>
    );

    const Showcase = () => {
      if ((siteConfig.users || []).length === 0) {
        return null;
      }

      const showcase = siteConfig.users
        .filter(user => user.pinned)
        .map(user => (
          <a href={user.infoLink} key={user.infoLink}>
            <img src={user.image} alt={user.caption} title={user.caption} />
          </a>
        ));

      const pageUrl = page => baseUrl + (language ? `${language}/` : '') + page;

      return (
        <div className="productShowcaseSection paddingBottom">
          <h2>Who is Using This?</h2>
          <p>This project is used by all these people</p>
          <div className="logos">{showcase}</div>
          <div className="more-users">
            <a className="button" href={pageUrl('users.html')}>
              More {siteConfig.title} Users
            </a>
          </div>
        </div>
      );
    };

    return (
      <div>
        <HomeSplash siteConfig={siteConfig} language={language} />
        <div className="mainContainer">
          <Features />
          <TryOut />
        </div>
      </div>
    );
  }
}

module.exports = Index;
